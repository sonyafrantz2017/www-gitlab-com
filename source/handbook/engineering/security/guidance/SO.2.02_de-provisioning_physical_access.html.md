---
layout: markdown_page
title: "SO.2.02 - De-provisioning Physical Access Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SO.2.02 - De-provisioning Physical Access

## Control Statement

Physical access that is no longer required in the event of a termination or role change is revoked. If applicable, temporary badges are returned prior to exiting facility.

## Context

This control refers to physical access to GitLab datacenters or facilities.

## Scope

This control is not applicable since GitLab has no datacenters or facilities.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.02_de-provisioning_physical_access.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.02_de-provisioning_physical_access.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.02_de-provisioning_physical_access.md).

## Framework Mapping

* ISO
  * A.11.1.2
* SOC2 CC
  * CC6.4
* PCI
  * 9.2
  * 9.3
  * 9.4.3
  * 9.5
