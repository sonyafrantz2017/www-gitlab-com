module Gitlab
  module Homepage
    class OssProject
      def initialize(data)
        @data = data
      end

      # rubocop:disable Style/MethodMissingSuper
      # rubocop:disable Style/MissingRespondToMissing

      ##
      # Middeman Data File objects compatibiltiy
      #
      def method_missing(name, *args, &block)
        @data[name.to_s]
      end

      # rubocop:enable Style/MethodMissingSuper
      # rubocop:enable Style/MissingRespondToMissing

      def self.all!
        @oss_projects ||= YAML.load_file('data/oss_projects.yml')
        @oss_projects.map do |data|
          new(data)
        end
      end
    end
  end
end
